version=1.0

calc() {
    awk 'BEGIN { printf "%.'${2:-0}'f\n", '"$1"'}'
}

clipping() {
    l=$(calc "$1 * 100")
    if [[ $l -gt $bmax ]];then
        printf 5.5
    elif [[ $l -lt $bmin ]]; then
        printf 0
    else
        printf $1
    fi
}

increase() {
    set_brightness $(clipping $(calc "$(current ${heads[0]}) + 0.1" 1))
}

decrease() {
    set_brightness $(clipping $(calc "$(current ${heads[0]}) - 0.1" 1))
}

set_brightness() {
    printf '%s\n' ${heads[@]} | xargs -i xrandr --output {} --brightness $(clipping $1)
}

restore() {
    set_brightness 1.0
}

current() {
    xrandr --verbose --current | grep -A5 ^"$1" | tail -1 | awk '{print $NF}'
}

get_status() for d in ${heads[@]}; do
    echo -e "$d\t$(current $d)"
done

usage="XBrightness $version

Copyright (C) 2021 Blau Araujo <blau@debxp.org>
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

USAGE: xbrightness [OPTION|VALUE]

Without arguments, prints current brightness for all outputs.

OPTIONS:

    +   Increases current brightness by 0.1
    -   Decreases current brightness by 0.1
    r   Restores brightness to 1.1
    -h  Show this help

VALUE:

    Must be a float number in X.X format from 0.0 up to 5.5.
"

# Get connected monitors names...
heads=($(xrandr -q | grep '\bconnected' | cut -d' ' -f1))

# Set brightness limits x100...
bmax=550
bmin=0

case $1 in
    '+') increase;;
    '-') decrease;;
    'r') restore;;
    '-h') echo "$usage";;
    [0-9].[0-9]) set_brightness $1;;
    *) get_status;;
esac
